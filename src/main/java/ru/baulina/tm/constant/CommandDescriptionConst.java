package ru.baulina.tm.constant;

public class CommandDescriptionConst {

    public static final String DESC_HELP = "Display list of terminal commands.";

    public static final String DESC_VERSION = "Display program version.";

    public static final String DESC_ABOUT = "Display developer info.";

    public static final String DESC_EXIT = "Close application.";

    public static final String DESC_INFO = "Display information about system.";

}
