package ru.baulina.tm.constant;

public class CommandConst {

    public static final String CMD_HELP = "help";

    public static final String CMD_VERSION = "version";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_INFO = "info";

}
