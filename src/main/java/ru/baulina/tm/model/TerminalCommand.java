package ru.baulina.tm.model;

import ru.baulina.tm.constant.ArgumentConst;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;

public class TerminalCommand {

    public static final TerminalCommand HELP = new TerminalCommand(
            CommandConst.CMD_HELP, ArgumentConst.ARG_HELP, CommandDescriptionConst.DESC_HELP
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            CommandConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, CommandDescriptionConst.DESC_ABOUT
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            CommandConst.CMD_VERSION, ArgumentConst.ARG_VERSION, CommandDescriptionConst.DESC_VERSION
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            CommandConst.CMD_INFO, ArgumentConst.ARG_INFO, CommandDescriptionConst.DESC_INFO
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            CommandConst.CMD_EXIT, null, CommandDescriptionConst.DESC_EXIT
    );

    private String name = "";

    private String arg = "";

    private String discription = "";

    public TerminalCommand(String name, String arg, String discription) {
        this.name = name;
        this.arg = arg;
        this.discription = discription;
    }

    public TerminalCommand(String name, String arg) {
        this.name = name;
        this.arg = arg;
    }

    public TerminalCommand(String name) {
        this.name = name;
    }

    public TerminalCommand() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();

        if (name != null && !name.isEmpty()) result.append(name);
        if (arg != null && !arg.isEmpty()) result.append(", ").append(arg);
        if (discription != null && !discription.isEmpty()) result.append(": ").append(discription);

        return result.toString();
    }

}
